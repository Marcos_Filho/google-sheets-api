const express = require("express")
const { google } = require("googleapis")

const alphabet = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]

const app = express()
app.use(express.json())

async function getAuthSheets() {

    const auth = new google.auth.GoogleAuth({
        keyFile: "credentials.json",
        scopes: "https://www.googleapis.com/auth/spreadsheets"
    })

    const client = await auth.getClient()

    const googleSheets = google.sheets({
        version: "v4",
        auth: client
    })

    const spreadsheetId = "1FzqUN7j_6YFkh4Hqj6h4MI6Zms7Glr9LHqgLG79yVSM"

    return {
        auth,
        client,
        googleSheets,
        spreadsheetId
    }
}

function getRange(sheetName, startColumnIndex, endColumnIndex) {
    return "'" + sheetName + "'!" + alphabet[startColumnIndex] + "1:" + alphabet[endColumnIndex - 2] + "99"
}

function formatArrayToJson(array) {
    var formatedJson = array
        .map((x) => {
            if (x.length > 0) {
                return JSON.parse(JSON.stringify(Object.assign({}, x)))
            }
        })
        .filter(notUndefined => notUndefined !== undefined)
    return formatedJson
}

function comparer(a, b) {
    if (a.startColumnIndex < b.startColumnIndex)
        return -1;

    if (a.startColumnIndex > b.startColumnIndex)
        return 1;

    return 0;
}


//Requests

app.get("/metadata", async (req, res) => {
    const { googleSheets, auth, spreadsheetId } = await getAuthSheets()

    const metadata = await googleSheets.spreadsheets.get({
        auth,
        spreadsheetId,
    })

    var sheets = []
    metadata.data.sheets.map((x, i) => {
        var sheet = {}
        sheet["sheetId"] = x.properties.sheetId
        sheet["title"] = x.properties.title

        var merges = x.merges.map((x) => {
            var merge = {}
            merge["startColumnIndex"] = x.startColumnIndex
            merge["endColumnIndex"] = x.endColumnIndex
            return merge
        })

        merges.sort(comparer)

        sheet["merges"] = merges
        sheets[i] = sheet
    })

    res.send(sheets)
})

app.get("/month", async (req, res) => {
    const { googleSheets, auth, spreadsheetId } = await getAuthSheets()

    let range = getRange(req.query.sheetName, req.query.startColumnIndex, req.query.endColumnIndex)

    const getFormulaRows = await googleSheets.spreadsheets.values.get({
        auth,
        spreadsheetId,
        valueRenderOption: "FORMULA",
        dateTimeRenderOption: "FORMATTED_STRING",
        range
    })

    let finalJson = {}
    let arrayBudgetInfo = []

    getFormulaRows.data.values.map((x) => {
        if (x.length < 1 || x[0] == '') {
            return
        }

        //set name
        if (finalJson.name == null) {
            finalJson["name"] = x[0]
            return
        }

        //set accounts
        if (finalJson.accounts == null) {
            finalJson["accounts"] = x.map((account, valueAccount) => {
                if (valueAccount == 0) return
                return account
            }).filter(notUndefined => notUndefined !== undefined)
            return
        }

        let budgetInfo = {}
        var values = []
        x.map((value, valueIndex) => {
            if (valueIndex == 0) {
                budgetInfo["description"] = value
                return
            }

            if (typeof value == "string") {
                if (value == "") {
                    value = []
                } else {
                    let formattedValue = value.replace(/,/g, ".").replace("=", "").split("+")
                    value = formattedValue.map(parseFloat)
                }
            } else {
                value = [value]
            }

            values[valueIndex - 1] = value
        })

        budgetInfo["value"] = values
        arrayBudgetInfo.push(budgetInfo)
    })

    finalJson["budgetInfos"] = arrayBudgetInfo

    res.send(finalJson)
})

//get last cell with border
app.get("/getLastCell", async (req, res) => {
    const { googleSheets, auth, spreadsheetId } = await getAuthSheets()

    const getFormattedRows = await googleSheets.spreadsheets.get(
        {
            spreadsheetId,
            fields: "sheets/data/rowData/values/userEnteredFormat/borders",
            ranges: "test!B1:B99"
        }
    )
    res.send(getFormattedRows.data.sheets[0].data[0].rowData.length + "")
})

app.post("/insertColumn", async (req, res) => {
    const { googleSheets, auth, spreadsheetId } = await getAuthSheets()

    const request_body = {
        'requests': [
            {
                'insertDimension': {
                    'range': {
                        'sheetId': 1368595774,
                        'dimension': 'COLUMNS',
                        'startIndex': 8,
                        'endIndex': 9
                    }
                }
            }
        ]
    }

    const insert = await googleSheets.spreadsheets.batchUpdate({
        spreadsheetId,
        requestBody: request_body
    })

    res.send(insert)
})


app.listen(3001, () => console.log("run"))